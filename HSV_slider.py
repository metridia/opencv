
  fig, ax = plt.subplots()
  plt.subplots_adjust(left=0.25, bottom=0.3)
  # t = np.arange(0.0, 1.0, 0.001)
  h = hue.max()+hue.max()*0.1
  s = sat.max()+sat.max()*0.1
  v = val.max()+val.max()*0.1
  
  
  l = ax.imshow(space_2_img)
  # plt.axis([0, 1, -10, 10])

  axcolor = 'lightgoldenrodyellow'
  h_freq = plt.axes([0.25, 0.1, 0.65, 0.03])#, facecolor='#FFFFCC')
  s_freq = plt.axes([0.25, 0.15, 0.65, 0.03])#, facecolor='#FFFFCC')
  v_freq = plt.axes([0.25, 0.2, 0.65, 0.03])#, facecolor='#FFFFCC')

  h_value = Slider(h_freq, 'Hue', hue.min(), h, valinit=np.average(hue))
  sat_value = Slider(s_freq, 'Sat', sat.min(), s, valinit=np.average(sat))
  v_value = Slider(v_freq, 'Value', val.min(),v, valinit=np.average(val))


  def update(val):
    h_new = h_value.val
    print "VAlueeeeeeeee: ",h_new
    sat_new = sat_value.val
    v_new = v_value.val
    newvals = np.array([0,0,0])
    mask = (space_2_img[:,:,0] < h_new) | (space_2_img[:,:,2]< sat_new) |(space_2_img[:,:,1]< v_new)
    new_img = space_2_img.copy()
    new_img.reshape(-1,3)[mask.ravel()] = newvals
    new_img = to_rgb(new_img)
    ax.imshow(new_img)
    fig.canvas.draw()
  h_value.on_changed(update)
  sat_value.on_changed(update)
  v_value.on_changed(update)

  resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
  button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')


  def reset(event):
      h_freq.reset()
      s_freq.reset()
      v_freq.reset()
  button.on_clicked(reset)

  rax = plt.axes([0.025, 0.5, 0.15, 0.15])#, facecolor=axcolor)
  radio = RadioButtons(rax, ('red', 'blue', 'green'), active=0)


  def colorfunc(label):
      l.set_color(label)
      fig.canvas.draw_idle()
  radio.on_clicked(colorfunc)

  plt.show()