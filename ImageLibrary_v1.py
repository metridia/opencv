from skimage import morphology
import matplotlib.pyplot as plt

import numpy as np
import time
import skimage
import sklearn.cluster as cluster
## Plot set-up ###
import seaborn as sns
sns.set_color_codes()
plot_kwds = {'alpha' : 0.25, 's' : 80, 'linewidths':0}
sns.set(style="white", palette="muted", color_codes=True)
sns.axes_style("whitegrid",{"legend.frameon": True,'lines.solid_capstyle': u'round','xtick.direction': u'in','ytick.direction': u'in'})
sns.set_style("ticks", {"xtick.major.size": 8, "ytick.major.size": 8})

sns.set_context("talk", font_scale=1.4)
# plt.style.use('seaborn-white')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams['text.latex.unicode'] = True
plot_kwds = {'alpha' : 0.15, 's' : 20, 'linewidths':1}
#######

import hdbscan # Can be installed in condas using conda install -c conda-forge hdbscan
import pandas as pd
from scipy.spatial import distance as dist
import scipy.cluster.hierarchy as hier
# from skimage.feature import corner_peaks, plot_matches
from skimage.exposure import equalize_adapthist
# from skimage.filters.rank import otsu
from skimage.filters import threshold_yen,gaussian_filter
import skimage.morphology as morph
import skimage.exposure as skie
import pylab as py
from matplotlib.widgets import SpanSelector
from skimage.color.adapt_rgb import adapt_rgb, each_channel, hsv_value


from skimage import io, color, exposure
from skimage import filters

from skimage.exposure import rescale_intensity

import matplotlib.pyplot as plt
import argparse
from scipy.cluster.vq import vq, kmeans,kmeans2
from matplotlib.widgets import Slider, Button, RadioButtons

def SmoothAndThreshold(image,limit):
	

	
	image_hsv = skimage.color.rgb2hsv(image)
	newvals = np.array([0.0,1,1])
	mask = (image_hsv[:,:,0] < limit)
	image_hsv.reshape(-1,3)[mask.ravel()] = newvals

	# py.imshow(image_hsv)
	# py.show()
	# raw_input('hold')
	image_hsv[:,:,2] = equalize_adapthist(image_hsv[:,:,2])
	image_rgb = skimage.color.hsv2rgb(image_hsv)
	gray_image = 	skimage.color.rgb2gray(image_rgb)
	
	# gray_image = gray_image<0.28
	gray_image = skie.adjust_gamma(gray_image) # Adjust exposure 

	# py.figure(1)
	# py.imshow(image)


	# py.figure(2)
	# py.imshow(gray_image)


	# py.figure(3)
	# py.hist(gray_image.ravel(),bins=256, histtype='step', color='black')
	# py.show()
	
	blur_size = 2
	blurred = gaussian_filter(gray_image, blur_size)
	highpass = gray_image - 0.98 * blurred
	sharp = blurred + highpass

	# equalized_gaussian_image = equalize_adapthist(image)
	adaptive_thresh = threshold_yen(sharp)
	# print adaptive_thresh
	# py.figure(0)
	# py.imshow(sharp)

	# py.figure(1)
	# py.imshow(highpass)

	# py.figure(2)
	# py.imshow(sharp)

	# py.figure(4)

	# py.imshow(sharp<=adaptive_thresh)

	# py.show()

	return sharp<=adaptive_thresh

def InitialSmoothAndThreshold(image1,image2):
	
	global limit
	smooth_size =3# pixels
	min_radius = 4
	max_radius = 20
	
	image1_hsv = skimage.color.rgb2hsv(image1)
	image2_hsv = skimage.color.rgb2hsv(image2)
	# Load Image and transform to a 2D numpy array.
	w1, h1, d1 = original_shape = tuple(image1_hsv.shape)
	w2, h2, d2 = original_shape = tuple(image2_hsv.shape)

	image1_array = np.reshape(image1_hsv, (w1 * h1, d1))
	image2_array = np.reshape(image2_hsv, (w1 * h1, d1))

	df_label1 = pd.DataFrame(columns={'label1','channel1'})
	df_label2 = pd.DataFrame(columns={'label1','channel1'})

	centroid1,df_label1['label1'] = kmeans2(np.array(image1_array[:,0], dtype=np.float), 1)
	centroid2,df_label2['label1'] = kmeans2(np.array(image2_array[:,0], dtype=np.float), 2)
	# centroid1, label1 = kmeans2(np.array(np.sqrt(image_array[:,1]*image_array[:,2]), dtype=np.float), 2)
	df_label1['channel1']=np.array(image1_array[:,0], dtype=np.float)
	df_label2['channel1']=np.array(image2_array[:,0], dtype=np.float)
	std_hue1 = df_label1.groupby('label1').agg({'channel1':['mean','std']})
	std_hue2 = df_label2.groupby('label1').agg({'channel1':['mean','std']})
	# from scipy.stats import norm
	# print std_hue1['channel1']['mean'],  std_hue1['channel1']['std']
	# print std_hue2['channel1']['mean'],  std_hue2['channel1']['std']
	# print std_hue['channel1']['mean']+std_hue['channel1']['std'],std_hue['channel1']['mean']+2*std_hue['channel1']['std']
	# print centroid1, label1

	sns.distplot(df_label1['channel1'].ravel(),color="r",label='NoSpecles')
	sns.distplot(df_label2['channel1'].ravel(),color="g",label='Specles')
	plt.show()
	limit =  np.float(raw_input('Based on the histogram put a limit value for hue: '))
	

	newvals = np.array([0.0,0.5,1])
	mask = (image1_hsv[:,:,0] <= limit)
	image1_hsv.reshape(-1,3)[mask.ravel()] = newvals


	mask = (image2_hsv[:,:,0] <= limit)
	image2_hsv.reshape(-1,3)[mask.ravel()] = newvals

	py.figure(1)
	py.imshow(skimage.color.hsv2rgb(image1_hsv))

	py.figure(2)
	py.imshow(skimage.color.hsv2rgb(image2_hsv))
	py.show()
	# raw_input('hold')
	# image_hsv[:,:,2] = equalize_adapthist(image_hsv[:,:,2])
	# image_rgb = skimage.color.hsv2rgb(image_hsv)
	# gray_image = 	skimage.color.rgb2gray(image_rgb)
	
	# # gray_image = gray_image<0.28
	# gray_image = skie.adjust_gamma(gray_image) # Adjust exposure 

	# # py.figure(1)
	# # py.imshow(image)


	# # py.figure(2)
	# # py.imshow(gray_image)


	# # py.figure(3)
	# # py.hist(gray_image.ravel(),bins=256, histtype='step', color='black')
	# # py.show()
	
	# blur_size = 3
	# blurred = gaussian_filter(gray_image, blur_size)
	# highpass = gray_image - 0.98 * blurred
	# sharp = blurred + highpass

	# # equalized_gaussian_image = equalize_adapthist(image)
	# adaptive_thresh = threshold_yen(sharp)
	# print adaptive_thresh
	# py.figure(0)
	# py.imshow(sharp)

	# py.figure(1)
	# py.imshow(highpass)

	# py.figure(2)
	# py.imshow(sharp)

	# py.figure(4)

	# py.imshow(sharp<=adaptive_thresh)

	# py.show()

	# return sharp<=adaptive_thresh


def plot_clusters(data, image,algorithm, args, kwds,index):
	start_time = time.time()
	labels = algorithm(*args, **kwds).fit_predict(data)
	end_time = time.time()
	# print labels
	# raw_input('hold')
	

	palette = sns.color_palette('deep', np.unique(labels).max() + 1)
	colors = [palette[x] if x >= 0 else (0.0, 0.0, 0.0) for x in labels]
	py.scatter(data.T[0], data.T[1], c=colors, **plot_kwds)
	frame = py.gca()
	frame.axes.get_xaxis().set_visible(False)
	frame.axes.get_yaxis().set_visible(False)

	df = pd.DataFrame(data, columns=['X', 'Y'])
	df['labels'] = labels

	centroids = df.groupby('labels').mean()
	n = len(centroids)
	# centers = algorithm.cluster_centers_
	py.scatter(centroids['X'], centroids['Y'], s=100, c=colors,alpha=0.5,linewidths=2)
	py.imshow(image)
	py.title('Clusters found by {}'.format(str(algorithm.__name__)), fontsize=24)
	py.text(-0.5, 0.7, 'Clusters : %d'%n, fontsize=14)
	py.savefig('Videos/Case1/Final_Clustered_%i'%index)
	return centroids

# def background_calibration(image):
# 	global secondary_plot 
# 	global primary_plot 
# 	global ax1,ax2,fig
# 	global _background_calibration, _background_calibration_hist,bin_edges,bincenters 

# 	_background_calibration = image
# 	_background_calibration_hist,bin_edges = np.histogram(image.ravel(),bins=256)
# 	bincenters = 0.5*(bin_edges[1:]+bin_edges[:-1])
# 	fig = py.figure()
# 	ax = fig.add_subplot(311)
# 	ax.imshow(_background_calibration)
	
# 	ax1 = fig.add_subplot(312)
# 	ax1.plot(bincenters,_background_calibration_hist, color='black')
# 	ax1.set_title('Press left mouse button and drag to test')

# 	ax2 = fig.add_subplot(313)
# 	ax2.imshow(_background_calibration)

# 	span = SpanSelector(ax1, onselect, 'horizontal', useblit=True, # Span selector for analysis
#                     rectprops=dict(alpha=0.5, facecolor='red'))

# 	py.tight_layout()
# 	py.show()
# def onselect(xmin, xmax):
	
# 	indmin, indmax = np.searchsorted(bincenters, (xmin, xmax))
# 	indmax = min(len(bincenters) - 1, indmax)
# 	# thisx = modi_time[indmin:indmax]
# 	val = bincenters[indmin: indmax]
# 	# print 
# 	mask = _background_calibration < val.max()
# 	ax2.imshow(mask)
# 	fig.canvas.draw()
# 	# thisx = x[indmin:indmax]
# 	# thisy = y[indmin:indmax]
# 	# line2.set_data(thisx, thisy)
# 	# ax2.set_xlim(thisx[0], thisx[-1])
# 	# ax2.set_ylim(thisy.min(), thisy.max())
# 	# fig.canvas.draw()
	
# 	# global gray_image
# 	# # edges = canny(equalized_gaussian_image/255.)
# 	# block_size = 35
# 	# equalized_gaussian_image = equalize_adapthist(gray_image)
# 	# adaptive_thresh = threshold_yen(equalized_gaussian_image)
# 	# ax1.imshow(equalized_gaussian_image <=adaptive_thresh)

	

# 	# T = threshold_otsu(gray_image)
# 	# ax3.imshow(gray_image < T)

# 	# # ax3.imshow(fill_particles)

# 	# limg = np.arcsinh(gray_image)
# 	# limg = limg / limg.max()
# 	# # ax3.imshow(limg)


# 	# low = np.percentile(limg, xlow*100,interpolation='linear')
# 	# high = np.percentile(limg, xhigh*100,interpolation='linear')

# 	# opt_img  = skie.exposure.rescale_intensity(limg, in_range=( xlow,xhigh))
# 	# # py.figure(9)
# 	# ax10.imshow(opt_img)
# 	# lm = morph.local_minima(opt_img)
# 	# x1, y1 = np.where(lm.T == True)
# 	# v = opt_img[(y1,x1)]
# 	# lim = 0.95
# 	# x2, y2 = x1[v < lim], y1[v < lim]
	
# 	# # py.figure(7)
# 	# ax30.imshow(limg,cmap='gray')
# 	# ax30.scatter(x2, y2, s=80, facecolors='none', edgecolors='r')
# 	# fig.canvas.draw()
# @adapt_rgb(each_channel)
# def sobel_each(image):
# 	return filters.sobel(image)


# # @adapt_rgb(hsv_value)
# def sobel_hsv(image):
# 	return filters.sobel(image)


# def color_filtering(image):
# 	# fig_col = py.figure(figsize=(14, 7))
# 	# ax_each = fig_col.add_subplot(121, adjustable='box-forced')
# 	# ax_hsv = fig_col.add_subplot(122, sharex=ax_each, sharey=ax_each,
# 	#                          adjustable='box-forced')

# 	# # We use 1 - sobel_each(image)
# 	# # but this will not work if image is not normalized
# 	# ax_each.imshow(rescale_intensity(1 - sobel_each(image)))
# 	# ax_each.set_xticks([]), ax_each.set_yticks([])
# 	# ax_each.set_title("Sobel filter computed\n on individual RGB channels")

# 	# # We use 1 - sobel_hsv(image) but this will not work if image is not normalized
# 	# ax_hsv.imshow(rescale_intensity(1 - sobel_hsv(image)))
# 	# ax_hsv.set_xticks([]), ax_hsv.set_yticks([])
# 	# ax_hsv.set_title("Sobel filter computed\n on (V)alue converted image (HSV)")
# 	# py.show()
# 	# reshape the image to be a list of pixels
# 	image = image.reshape((image.shape[0] * image.shape[1], 3))

# 	# cluster the pixel intensities
# 	clt = cluster.KMeans(n_clusters = 2)
# 	clt.fit(image)
# 	return clt

# def plot_colors(hist, centroids):
# 	# initialize the bar chart representing the relative frequency
# 	# of each of the colors
# 	bar = np.zeros((50, 300, 3), dtype = "uint8")
# 	startX = 0
#  	import cv2
# 	# loop over the percentage of each cluster and the color of
# 	# each cluster
# 	print hist,centroids
# 	raw_input('hold')
# 	for (percent, color) in zip(hist, centroids):
# 		# plot the relative percentage of each cluster
# 		endX = startX + (percent * 300)
# 		cv2.rectangle(bar, (int(startX), 0), (int(endX), 50),
# 			color.astype("uint8").tolist(), -1)
# 		startX = endX
	
# 	# return the bar chart
# 	return bar

# def centroid_histogram(clt):

# 	# grab the number of different clusters and create a histogram
# 	# based on the number of pixels assigned to each cluster
# 	numLabels = np.arange(0, len(np.unique(clt.labels_)) + 1)
# 	(hist, _) = np.histogram(clt.labels_, bins = numLabels)
 
# 	# normalize the histogram, such that it sums to one
# 	hist = hist.astype("float")
# 	hist /= hist.sum()
 
# 	# return the histogram
# 	return hist

# def do_cluster(hsv_image, K, channels):
# 	# gets height, width and the number of channes from the image shape
# 	h,w,c = hsv_image.shape
# 	# prepares data for clustering by reshaping the image matrix into a (h*w) x c matrix of pixels
# 	cluster_data = hsv_image.reshape( (h*w,c) )
# 	# grabs the initial time
# 	t0 = time.time()
# 	# performs clustering
# 	centroid, label = kmeans2(np.array(cluster_data[:,channels-1], dtype=np.float), K)
# 	print centroid, label
# 	# # takes the final time
# 	t1 = time.time()
# 	print "Clusterization took %0.5f seconds" % (t1-t0)

# 	return np.sort(centroid),label,cluster_data[:,channels-1]

# 	# # calculates the total amount of pixels
# 	# tot_pixels = h*w
# 	# # generates clusters
# 	# data, dist = vq(cluster_data[:,0:channels], codebook) # dist: values

# 	# print data
# 	# print dist

# 	# df = pd.DataFrame(dist)

# 	# print df.head()
# 	# raw_input('hold')

# 	# # def get_image_and_mask(cluster):
# 	#    #  mask = labels == cluster
# 	#    #  # some morphology stuff really cleans up the output picture
# 	#    #  mask = morphology.binary_closing(mask, morphology.diamond(1))
# 	#    #  mask = morphology.binary_opening(mask, morphology.diamond(1))
# 	#    #  # Binary opening/closing outputs non binary values
# 	#    #  mask = mask.astype(np.bool)
# 	#    #  img_cluster = space_2_img.copy()

# 	#    #  # Darken the points that are not in the cluster by down
# 	#    #  # scaling the L component and knocking out the color
# 	#    #  # components
# 	#    #  img_cluster[..., brightness_dimension][~mask] *= .5
# 	#    #  # knock out the non brightness components
# 	#    #  for d in space_2_dimensions:
# 	#    #      img_cluster[:,:, d][~mask] = 0

# 	#    #  to_return = image.copy()
# 	#    #  to_return[~mask] = to_rgb(img_cluster)[~mask]
# 	#    #  py.figure(211)
# 	#    #  py.imshow(to_return)
# 	#    #  print cluster
# 	#    #  print to_return[~mask]
# 	#    #  py.show()
# 	#    #  return (to_return, mask)
# 	# # calculates the number of elements for each cluster
# 	# weights = [len(data[data == i]) for i in range(0,K)]

# 	# # creates a 4 column matrix in which the first element is the weight and the other three
# 	# # represent the h, s and v values for each cluster
# 	# color_rank = np.column_stack((weights, codebook))
# 	# # sorts by cluster weight

# 	# color_rank = color_rank[np.argsort(color_rank[:,0])]
# 	# # print color_rank[:,0:K+1]
# 	# # creates a new blank image
# 	# new_image =  np.array([0,0,255], dtype=np.uint8) * np.ones( (500, 500, 3), dtype=np.uint8)
# 	# img_height = new_image.shape[0]
# 	# img_width  = new_image.shape[1]

# 	# # for each cluster
# 	# for i,c in enumerate(color_rank[::-1]):

# 	# 	# gets the weight of the cluster
# 	# 	weight = c[0]

# 	# 	# calculates the height and width of the bins
# 	# 	height = int(weight/float(tot_pixels) *img_height )
# 	# 	width = img_width/len(color_rank)

# 	# 	# calculates the position of the bin
# 	# 	x_pos = i*width



# 	# 	# defines a color so that if less than three channels have been used
# 	# 	# for clustering, the color has average saturation and luminosity value
# 	# 	color = np.array( [0,128,200], dtype=np.uint8)

# 	# 	# substitutes the known HSV components in the default color
# 	# 	for j in range(len(c[1:])):
# 	# 	    color[j] = c[j+1]

# 	# 	# draws the bin to the image
# 	# 	new_image[ img_height-height:img_height, x_pos:x_pos+width] = [color[0], color[1], color[2]]
# 	# print color_rank[::-1]
#     # returns the cluster representation
# 	# return 	new_image

# def color_clustering_initial(image):
# 	def get_image_and_mask(cluster):
# 	    mask = labels == cluster
# 	    # some morphology stuff really cleans up the output picture
# 	    mask = morphology.binary_closing(mask, morphology.diamond(1))
# 	    mask = morphology.binary_opening(mask, morphology.diamond(1))
# 	    # Binary opening/closing outputs non binary values
# 	    mask = mask.astype(np.bool)
# 	    img_cluster = space_2_img.copy()

# 	    # Darken the points that are not in the cluster by down
# 	    # scaling the L component and knocking out the color
# 	    # components
# 	    img_cluster[..., brightness_dimension][~mask] *= .5
# 	    # knock out the non brightness components
# 	    for d in space_2_dimensions:
# 	        img_cluster[:,:, d][~mask] = 0

# 	    to_return = image.copy()
# 	    to_return[~mask] = to_rgb(img_cluster)[~mask]
# 	    py.figure(211)
# 	    py.imshow(to_return)
# 	    print cluster
# 	    print to_return[~mask]
# 	    py.show()
# 	    return (to_return, mask)
# 	CLUSTERS = int(raw_input('Enter possible number of colors in integer:'))
# 	# We are going to cluster in a different space than rgb.
# 	(to_other_space, to_rgb) = (color.rgb2hsv, color.hsv2rgb)
# 	space_2_dimensions = [1, 2]
# 	brightness_dimension = 0
# 	(space_min, space_max) = (-100, 100)
# 	# convert to HSV colorspace
# 	space_2_img = to_other_space(image)
# 	py.imshow(space_2_img)
# 	py.show()
# 	# We can plot the histograms 
# 	# by selecting one column at the time from the image matrix representation.
# 	hue, sat, val = space_2_img[:,:,0], space_2_img[:,:,1], space_2_img[:,:,2]
# 	# print hue.min(),hue.max()
# 	# plt.figure(figsize=(10,8))
# 	# plt.subplot(311)                             #plot in the first cell
# 	# plt.subplots_adjust(hspace=.5)
# 	# plt.title("Hue")
# 	# plt.hist(np.ndarray.flatten(hue), bins=180)
# 	# plt.subplot(312)                             #plot in the second cell
# 	# plt.title("Saturation")
# 	# plt.hist(np.ndarray.flatten(sat), bins=128)
# 	# plt.subplot(313)                             #plot in the third cell
# 	# plt.title("Luminosity Value")
# 	# plt.hist(np.ndarray.flatten(val), bins=128)
# 	# plt.show()

# 	# hue_clip = 0.13
# 	# newvals = np.array([0,0,0])
# 	# mask = (space_2_img[:,:,0] < 0.13) | (space_2_img[:,:,2]> 200)
# 	# space_2_img.reshape(-1,3)[mask.ravel()] = newvals

	



# 	# creates a new figure size 12x10 inches
# 	# plt.figure(figsize=(12,10))
# 	# creates a 4-column subplot
# 	# plt.subplot(141)
# 	# in the first cell draws the target image
# 	# plt.imshow(space_2_img)

# 	# calculates clusters for
# 	# * h
# 	# * h and s
# 	# * h, s and v
# 	df = pd.DataFrame(columns={'channel1','channel2','channel3'})
# 	df_label = pd.DataFrame(columns={'label1','label2','label3','channel1','channel2','channel3'})
# 	for i in range(1,4):
# 	    # plt.subplot(141 + i)
# 	    plt.title("Channels: %i" % i)
# 	    # new_image = do_cluster(space_2_img, CLUSTERS, i)
# 	    df['channel%i'%i],df_label['label%i'%i],df_label['channel%i'%i]=do_cluster(space_2_img, CLUSTERS, i)
# 	    # print df

# 	    # new_image = cv2.cvtColor(new_image, cv2.COLOR_HSV2RGB)
# 	    # print new_image
# 	    # new_image = to_rgb(new_image)
# 	    # plt.imshow(new_image)
# 	# plt.show()
# 	hue_clip = df['channel1'][0]
# 	sat_clip = df['channel2'][0]
# 	val_clip = df['channel3'][0]

# 	std_hue = df_label.groupby('label1').agg({'channel1':['mean','std']})
# 	std_sat = df_label.groupby('label2').agg({'channel2':['mean','std']})
# 	std_val = df_label.groupby('label3').agg({'channel3':['mean','std']})

# 	# print std_hue['mean'][0],std_sat['mean'][0],std_val['mean'][0]
# 	print std_hue#,std_sat,std_val
# 	print std_hue['channel1']['mean']
	
	
# 	markers = np.copy(space_2_img)
# 	# markers = np.ma.masked_where((x > l) & (x < u), x)


# 	# markers = np.zeros(space_2_img.shape,dtype=np.uint8)
# 	# markers[:,:,0 > (std_hue['channel1']['mean'][0]+std_hue['channel1']['std'][0])]=1
# 	# markers[:,:,0 < (std_hue['channel1']['mean'][0]-std_hue['channel1']['std'][0])]=1

# 	# markers[:,:,1 > (std_sat['channel2']['mean'][0]+std_sat['channel2']['std'][0])]=1
# 	# markers[:,:,1 < (std_sat['channel2']['mean'][0]-std_sat['channel2']['std'][0])]=1

# 	# markers[:,:,2 > (std_val['channel3']['mean'][0]+std_val['channel3']['std'][0])]=0.1
# 	# markers[:,:,2 < (std_val['channel3']['mean'][0]-std_val['channel3']['std'][0])]=0.1

# 	# markers[:,:,2] = [0 if (markers[:,:,0] > (std_hue['channel1']['mean'][0]+std_hue['channel1']['std'][0])) & (markers[:,:,0] < (std_hue['channel1']['mean'][0]-std_hue['channel1']['std'][0]))]
# 	# a = (markers[:,:,0] > (std_hue['channel1']['mean'][0]+std_hue['channel1']['std'][0]))

# 	# print markers[0,0,0]
# 	for i,val1 in enumerate(markers[:,:,0]):
# 		for j,val2 in enumerate(val1):
# 			if val2 > (std_hue['channel1']['mean'][0]+std_hue['channel1']['std'][0]) or val2 < (std_hue['channel1']['mean'][0]-std_hue['channel1']['std'][0]):
# 				markers[i,j,2] = 0



# 	# newvals = np.array([0,0,0])
# 	# mask = (space_2_img[:,:,0] < hue_clip) | (space_2_img[:,:,1]< sat_clip) | (space_2_img[:,:,2]< val_clip)
# 	# space_2_img.reshape(-1,3)[mask.ravel()] = newvals
# 	plt.imshow(to_rgb(markers))
# 	plt.show()

# 	raw_input('hold')



# 	# name_and_rgb[::len(name_and_rgb) // 15]
# def draw_for_cluster(cluster, n_rows, n_cols, start_index):
# 	"""Plot the information about the cluster, and return some data."""

# 	(as_rgb, mask) = get_image_and_mask(cluster)

# 	color_counts = top_n_colors(as_rgb[mask], num_results=NUM_COLORS)

# 	plt.subplot(n_rows, n_cols, start_index)
# 	plt.imshow(as_rgb)
# 	plt.axis('off')

# 	# Draw the histogram
# 	(color_labels, counts) = zip(*color_counts)
# 	counts = np.array(counts)
# 	counts = counts / np.linalg.norm(counts)

# 	plt.subplot(n_rows, n_cols, start_index + 1)
# 	plt.axis('on')
# 	plt.bar(np.arange(NUM_COLORS), counts)
# 	plt.xticks(np.arange(NUM_COLORS), color_labels, rotation=20)
# 	plt.tick_params(labelsize=12)

# def top_n_colors(pixels, num_results=10, debug=False):
# 	global rounded_image
# 	if pixels.max() < 1:
# 		# We are in [0-1] color range
# 		pixels = pixels * 256
# 	# (_, colors) = color_tree.query(pixels)
# 	print pixels
# 	raw_input('hold')
# 	if debug:
# 		rounded_image = rgb_array.take(colors, axis=0)

# 	counts = np.bincount(colors.flatten())
# 	top_k = np.argsort(counts)[-num_results:][::-1]
# 	return [(color_names[i], counts[i]) for i in top_k]

def Understand_FFT(image,image2,index):


	if index==0:
		filtered_image = InitialSmoothAndThreshold(image,image2)

	A = np.array_split(image,3,axis=0) # Split the image into 3 regions verically, this helps localizing the shadows

	final = []
	for images in A:
	
		filtered_image = SmoothAndThreshold(images,limit) # Perform smoothing and thresholding on the spilt images
		final.append(filtered_image)



	final_image = np.concatenate(final,axis=0) # Joining the threshold images back

	limg = final_image 
	limg = limg / limg.max() # Rescale iamge values between 0 and 1
	

	lm = morph.local_maxima(limg) # Finiding the local maxima (White areas or our specles)
	x1, y1 = np.where(lm.T == True)
	v = limg[(y1,x1)]
	lim = 0.90
	x2, y2 = x1, y1
	positions = np.column_stack((x2, y2)) # Stack the coordinates as single array

	

	py.figure(2)
	py.imshow(image,cmap='Greys')
	py.scatter(x2, y2, s=80, facecolors='none', edgecolors='r')
	py.savefig('Videos/Case1/Actual_%i'%index)

	py.figure(21)
	py.imshow(final_image,cmap='Greys')
	py.scatter(x2, y2, s=80, facecolors='none',alpha=0.25, edgecolors='r')
	py.tight_layout(True)
	py.savefig('Videos/Case1/Final_%i'%index)


	"""
	 Different CLustering commands 
	 that can be used to test other clustering algorithms

	"""
	# plot_clusters(positions, cluster.KMeans, (), {'n_clusters':6})
	# plot_clusters(positions, cluster.MeanShift, (0.5,), {'cluster_all':True})
	# plot_clusters(positions, cluster.DBSCAN, (), {'eps':0.5})

	
	centroids = plot_clusters(positions, final_image, hdbscan.HDBSCAN, (), {'min_cluster_size':10},index)

	return len(centroids)



def get_base1(image,index,xmin,xmax,ymin,ymax,image2): 
	global limit
	Understand_FFT(image,image2,index)
	



	

	
	# # ax2.hist(image[:,:][1])
	# # ax2.hist(image[:,:][2])
	# ax0.imshow(image)
	# # mask = image < image.mean()
	# # image[mask] = 255
	# ax1.imshow(gray_image, cmap='gray')
	# print np.shape(gray_image)
	# print gray_image

	# from skimage.exposure import equalize_hist
	# from skimage.filters.rank import otsu
	# from skimage.filters import threshold_otsu

	# radius = 10
	# selem = disk(radius)

	# # # t_loc_otsu is an image
	# # t_loc_otsu = otsu(gray_image, selem)
	# # loc_otsu = gray_image <= t_loc_otsu

	# # # t_glob_otsu is a scalar
	# # t_glob_otsu = threshold_otsu(gray_image)
	# # glob_otsu = gray_image <= t_glob_otsu

	# equalized_image = equalize_hist(gray_image)

	# # loc_otsu = ndi.gaussian_filter(loc_otsu, sigma=2)

	# ax2.imshow(equalized_image,cmap='gray')

	# gaussian_image = ndi.gaussian_filter(gray_image, sigma=2)

	



	# # histo = np.histogram(equalized_image, bins=np.arange(0,256))
	# # ax5.hist(histo[0],histo[1])
	# # plot_img_and_hist(equalized_image,ax5)

	# # binary_image = np.where(gray_image > np.mean(gray_image),1.0,0.0)

	# ax3.imshow(gaussian_image,cmap='gray')
	# # plot_img_and_hist(binary_image,ax6)

	# # plot_img_and_hist(gray_image,ax7)
	# # plt.show()
	# equalized_gaussian_image = equalize_hist(gaussian_image)

	# binary_image = np.where(equalized_gaussian_image < 2*np.mean(equalized_gaussian_image),1.0,0.0)
	
	# limg = np.arcsinh(binary_image)
	# limg = limg / limg.max()

	# low = np.percentile(limg, 2)
	# high = np.percentile(limg, 100)
	# opt_img  = skie.exposure.rescale_intensity(limg, in_range=(low,high))

	# lm = morph.local_minima(limg)
	# x1, y1 = np.where(lm.T == True)
	# v = limg[(y1,x1)]
	# lim = 0.95
	# x2, y2 = x1[v < lim], y1[v < lim]

	# ax7.imshow(binary_image,cmap='gray')
	# ax7.scatter(x2, y2, s=80, facecolors='none', edgecolors='r')
	# plt.show()

	# return len(x2)


