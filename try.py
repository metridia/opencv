
import ImageLibrary_v1 as lab

import pylab as py
import imageio
import numpy as np
# from skimage import data 
from scipy import stats
from scipy import ndimage

filename = 'Videos/DRY_CASE_45ANGLE_35RPM/RTD1.MOV'
vid = imageio.get_reader(filename,  'ffmpeg')

# nums = [1000, 1500]

# print vid, len(vid)
# raw_input('hold')
# nums = np.arange(900,10000)

conc = []
for i,image in enumerate(vid):

	if i >= 0*int(len(vid)):
		if i==0*int(len(vid)):
			
			# from pylab import *
			from matplotlib.widgets import LassoSelector
			lasso_vert= []
			fig, ax = py.subplots()
			ax.imshow(image,cmap=py.cm.Greys)

			def onselect(verts):
				lasso_vert.append(verts)

			lasso = LassoSelector(ax, onselect)


			# subplots_adjust(left=0.1, bottom=0.1) 
			ax.set_title('User mouse to selct a region')
			py.show()

			lasso_vert = np.array(lasso_vert[0])
			# print lasso_vert

			ymin = int(min(lasso_vert[:,0]))
			xmin = int(min(lasso_vert[:,1]))

			ymax = int(max(lasso_vert[:,0]))
			xmax = int(max(lasso_vert[:,1]))

			image_final = image[xmin:xmax,ymin:ymax]

			py.imshow(image_final,cmap=py.cm.Greys)
			py.show()

		# image = vid.get_data(i)
		# fig = plt.figure()
		# fig.suptitle('image #{}'.format(num), fontsize=20)
		# plt.imshow(image)
		# plt.show()
		# conc.append(lab.get_base1(image))
		# xmin = 250
		# xmax =550 
		# ymin =250
		# ymax = 550
		lab.get_base1(image_final,i,xmin,xmax,ymin,ymax)
		# base_angle, rsq = lab.get_base1(image)
		# image_r = lab.rotate_image(image, base_angle)
