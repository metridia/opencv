from skimage import morphology
import matplotlib.pyplot as plt
from scipy import ndimage
import numpy as np
from skimage import io
import skimage

## Plot set-up ###
import seaborn as sns
sns.set(style="white", palette="muted", color_codes=True)
sns.axes_style("whitegrid",{"legend.frameon": True,'lines.solid_capstyle': u'round','xtick.direction': u'in','ytick.direction': u'in'})
sns.set_style("ticks", {"xtick.major.size": 8, "ytick.major.size": 8})

sns.set_context("talk", font_scale=1.4)
# plt.style.use('seaborn-white')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams['text.latex.unicode'] = True
#######


# from skimage import exposure
# from skimage import filters
# from scipy import stats
# from skimage.transform import rotate 
# import cPickle as pickle
# import os
# import matplotlib.image as mpimg

# from skimage.filters.rank import entropy
# from skimage.morphology import disk
# from skimage.feature import corner_peaks, plot_matches
# from skimage.filters import threshold_otsu, threshold_local

# import matplotlib.pyplot as mpl
# import numpy as np
# # import pyfits
# import skimage.morphology as morph
# import skimage.exposure as skie
# from scipy import ndimage as ndi


from scipy import fftpack
# # import pyfits
# import numpy as np
import pylab as py
# import radialProfile

def azimuthalAverage(image, center=None):
	"""
	Calculate the azimuthally averaged radial profile.

	image - The 2D image
	center - The [x,y] pixel coordinates used as the center. The default is 
	         None, which then uses the center of the image (including 
	         fracitonal pixels).

	"""
	# Calculate the indices from the image
	y, x = np.indices(image.shape)

	if not center:
	    center = np.array([(x.max()-x.min())/2.0, (x.max()-x.min())/2.0])

	r = np.hypot(x - center[0], y - center[1])

	# Get sorted radii
	ind = np.argsort(r.flat)
	r_sorted = r.flat[ind]
	i_sorted = image.flat[ind]

	# Get the integer part of the radii (bin size = 1)
	r_int = r_sorted.astype(int)

	# Find all pixels that fall within each radial bin.
	deltar = r_int[1:] - r_int[:-1]  # Assumes all radii represented
	rind = np.where(deltar)[0]       # location of changed radius
	nr = rind[1:] - rind[:-1]        # number of radius bin

	# Cumulative sum to figure out sums for each radius bin
	csim = np.cumsum(i_sorted, dtype=float)
	tbin = csim[rind[1:]] - csim[rind[:-1]]

	radial_prof = tbin / nr

	return radial_prof

def plot_img_and_hist(image,axes, bins=256):
	"""Plot an image along with its histogram and cumulative histogram.

	"""
	# image = img_as_float(image)
	# ax_img, ax_hist = axes
	axes = axes.twinx()

	# Display image
	# ax_img.imshow(image, cmap=plt.cm.gray)
	# ax_img.set_axis_off()
	# ax_img.set_adjustable('box-forced')

	# Display histogram
	axes.hist(image.ravel(), bins=bins, color='black')
	axes.ticklabel_format(axis='y', style='scientific', scilimits=(0, 0))
	axes.set_xlabel('Pixel intensity')
	axes.set_xlim(0, 1)
	axes.set_yticks([])

	# Display cumulative distribution
	img_cdf, bins = exposure.cumulative_distribution(image, bins)
	axes.plot(bins, img_cdf, 'r')
	axes.set_yticks([])

def show_curve(array):
	""" Plots real and imaginary parts of an array """

	ax = plt.subplot(111)
	ax.set_xlim(0, len(array))
	ax.set_ylim(-10, len(array))
	plt.plot(np.real(array), color='blue')
	plt.plot(np.imag(array), color='red')

def img2curve(imgfile, arrayfile):
	""" Converts an image of a curve into numpy array and saves it"""

	img = io.imread(imgfile)
	img = color.rgb2gray(img)
	img = util.img_as_ubyte(img)
	array = np.zeros((img.shape[1], ))

	for r in range(img.shape[0]):
	    for c in range(img.shape[1]):
	        if img[r, c] < 128:
	            array[c] = img.shape[0] - r
	np.save(arrayfile, array)

def Understand_FFT(image):
	
	gray_image = 	skimage.color.rgb2gray(image)

	# Take the fourier transform of the image.
	# F1 = fftpack.fft2(gray_image)
	 
	""" Now shift the quadrants around so that low spatial frequencies are in
	 the center of the 2D fourier transformed image.
	"""

	# F2 = fftpack.fftshift( F1 )
	# print np.shape(F2),F2[0][0]

	# raw_input('hold')

	
	 
	# Calculate a 2D power spectrum
	# psd2D = np.abs( F2 )**2
	 
	# Calculate the azimuthally averaged 1D power spectrum
	# psd1D = azimuthalAverage(psd2D)
	 
	# Now plot up both

	py.figure(1)	
	py.clf()
	py.imshow( image)
	py.title('Actual', y=1.08)
	# py.savefig('Actual_image_with_CUT.eps', format='eps', dpi=1000)# plt.ion()

	py.figure(2)
	py.clf()
	py.imshow( gray_image,cmap=py.cm.Greys)
	py.title('Actual(Gray scale)')
	# py.savefig('Actual_image_with_CUTGray.eps', format='eps', dpi=1000)# plt.ion()

	py.figure(3)
	py.clf()
	py.imshow( np.log10( gray_image ), cmap=py.cm.Greys)
	py.title('Actual in log(Gray scale)')
	# py.savefig('Actual_image_with_noCUTGray_log.eps', format='eps', dpi=1000)# plt.ion()

	# py.figure(4)
	# py.clf()
	# py.imshow( np.log10( psd2D ),cmap='gray')

	# py.title('Lograthmic 2D power spectrum ')
	# # py.savefig('Actual_image_with_CUTGray_2D_power_spectrum.eps', format='eps', dpi=1000)# plt.ion()

	 
	# py.figure(5)
	# py.clf()
	# py.semilogy( psd1D )
	# py.xlabel('Spatial Frequency')
	# py.ylabel('Power Spectrum')
	# py.title('1D power spectrum ')

	#################
	# rows, cols = gray_image.shape
	# crow,ccol = rows/2 , cols/2
	# F2[crow-crow:crow+crow, ccol-ccol:ccol+ccol] = 0.5
	# f_ishift = np.fft.ifftshift(F2)
	# img_back = np.fft.ifft2(f_ishift)
	# img_back = np.abs(img_back)

	###################

	py.figure(6)
	py.clf()
	py.imshow( img_back )
	
	py.title('img_back ')

	# py.savefig('Actual_image_with_CUTGray_1D_power_spectrum.eps', format='eps', dpi=1000)# plt.ion()

	py.show()


def get_base1(image,index,xmin,xmax,ymin,ymax): 
	
	if index !=0:
		image = image[xmin:xmax,ymin:ymax]

	# fig, [(ax0, ax1,ax2,ax3),(ax4, ax5,ax6,ax7)] = plt.subplots(nrows=2, ncols=4, figsize=(20, 8))

	Understand_FFT(image)
	



	

	
	# # ax2.hist(image[:,:][1])
	# # ax2.hist(image[:,:][2])
	# ax0.imshow(image)
	# # mask = image < image.mean()
	# # image[mask] = 255
	# ax1.imshow(gray_image, cmap='gray')
	# print np.shape(gray_image)
	# print gray_image

	# from skimage.exposure import equalize_hist
	# from skimage.filters.rank import otsu
	# from skimage.filters import threshold_otsu

	# radius = 10
	# selem = disk(radius)

	# # # t_loc_otsu is an image
	# # t_loc_otsu = otsu(gray_image, selem)
	# # loc_otsu = gray_image <= t_loc_otsu

	# # # t_glob_otsu is a scalar
	# # t_glob_otsu = threshold_otsu(gray_image)
	# # glob_otsu = gray_image <= t_glob_otsu

	# equalized_image = equalize_hist(gray_image)

	# # loc_otsu = ndi.gaussian_filter(loc_otsu, sigma=2)

	# ax2.imshow(equalized_image,cmap='gray')

	# gaussian_image = ndi.gaussian_filter(gray_image, sigma=2)

	



	# # histo = np.histogram(equalized_image, bins=np.arange(0,256))
	# # ax5.hist(histo[0],histo[1])
	# # plot_img_and_hist(equalized_image,ax5)

	# # binary_image = np.where(gray_image > np.mean(gray_image),1.0,0.0)

	# ax3.imshow(gaussian_image,cmap='gray')
	# # plot_img_and_hist(binary_image,ax6)

	# # plot_img_and_hist(gray_image,ax7)
	# # plt.show()
	# equalized_gaussian_image = equalize_hist(gaussian_image)

	# binary_image = np.where(equalized_gaussian_image < 2*np.mean(equalized_gaussian_image),1.0,0.0)
	
	# limg = np.arcsinh(binary_image)
	# limg = limg / limg.max()

	# low = np.percentile(limg, 2)
	# high = np.percentile(limg, 100)
	# opt_img  = skie.exposure.rescale_intensity(limg, in_range=(low,high))

	# lm = morph.local_minima(limg)
	# x1, y1 = np.where(lm.T == True)
	# v = limg[(y1,x1)]
	# lim = 0.95
	# x2, y2 = x1[v < lim], y1[v < lim]

	# ax7.imshow(binary_image,cmap='gray')
	# ax7.scatter(x2, y2, s=80, facecolors='none', edgecolors='r')
	# plt.show()

	# return len(x2)


